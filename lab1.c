#include <stdio.h>

int fibonacci(int n) {
if (n <= 0) {
printf("Ошибка: Введите положительное целое число.\n");
return -1; // Возвращаем ошибку
} else if (n == 1)
return 0;
else if (n == 2)
return 1;

int a = 0, b = 1;
for (int i = 3; i <= n; i++) {
int next = a + b;
a = b;
b = next;
}
return b;
}

int main() {
int n;
printf("Введите номер числа в последовательности Фибоначчи: ");
scanf("%d", &n);
int result = fibonacci(n);
if (result != -1)
printf("%d-е число в последовательности Фибоначчи: %d\n", n, result);
return 0;
}
